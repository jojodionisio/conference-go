import requests
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import json

def get_city_photo(city, state):
    #remove initial req.get bc you have one after query
    # requests.get('https://api.pexels.com/v1/search')
    #eventually build to city/state
    #can set url or not bc it's above
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    query = {
        "query": f'{city} {state}',
        'per_page' : 1,
    }
    response = requests.get(url, params=query, headers=headers)
    # print("you made it here", response)
    # return response
    photo_dict =json.loads(response.content)
    try:
        return photo_dict['photos'][0]['url']
    except (KeyError, IndexError):
        return None

def get_weather_data(city, state):
    query = f'{city},{state},US'
    api_key = OPEN_WEATHER_API_KEY
    #gonna populate after its called
    geocode_url = f'http://api.openweathermap.org/geo/1.0/direct?q={query}&appid={api_key}'
#now want a response from it; make a var that requests geocode url to get
#dict info
#make sure you use GEOCODE url bc specific to this
    geocode_response = requests.get(geocode_url)
    #want to get lat and long bc req to get weather data
    #get lat and long to send to url
    geocode_data = geocode_response.json()
    #autofills w data we're getting ^
    lat = geocode_data[0]['lat']
    lon = geocode_data[0]['lon']
    weather_url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api_key}'
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()
    temp = weather_data['main']['temp']
    #be able to see distinctions on how to access dicts vs lists
    #know that lists are accessed thru idx and dict thru keys
    description = weather_data['weather'][0]['description']

    #create dict so JSON can read it -> expects an OUTPUT so use dict
    weather_dict = {
        "temp": temp,
        "description": description,
    }
    return weather_dict
